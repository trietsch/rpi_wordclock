Robin's mod to rpi_wordclock
=============

Feature request list:
1. Change brightness according to time of day / position of the sun
2. Assign buttons to do something and not break the wordclock process
3. Adapt plugins to support Dutch 10 by 9 layout

Software to create a Raspberry Pi based wordclock. Original: https://github.com/bk1285/rpi_wordclock/

Documentation is available at http://rpi-wordclock.readthedocs.org/en/latest/index.html
