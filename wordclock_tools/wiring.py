import ast

import logging
import math


class wiring:
    '''
    A class, holding all information of the wordclock's layout to map given
    timestamps, 2d-coordinates to the corresponding LEDs (corresponding to
    the individual wiring/layout of any wordclock).
    If a different wordclock wiring/layout is chosen, this class needs to be
    adopted.
    '''

    def __init__(self, config):

        # LED strip configuration:
        language = config.get('stencil_parameter', 'language')
        stencil_content = ast.literal_eval(config.get('language_options', language))
        self.WCA_HEIGHT = len(stencil_content)
        self.WCA_WIDTH = len(stencil_content[0].decode('utf-8'))
        self.LED_COUNT = self.WCA_WIDTH * self.WCA_HEIGHT  # Number of LED pixels.
        self.LED_PIN = 18  # GPIO pin connected to the pixels (must support PWM!).
        self.LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
        self.LED_DMA = 5  # DMA channel to use for generating signal (try 5)
        self.LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)

        # Logger
        level = logging.getLevelName(config.get('logger', 'logging_level'))
        self.LOG = logging.getLogger(__name__)
        self.LOG.setLevel(level)

        self.LOG.info('Wiring configuration - WCA_WIDTH: ' + str(self.WCA_WIDTH))
        self.LOG.info('Wiring configuration - WCA_HEIGHT: ' + str(self.WCA_HEIGHT))
        self.LOG.info('Wiring configuration - Number of LEDs: ' + str(self.LED_COUNT))

        wiring_layout = config.get('wordclock_display', 'wiring_layout')
        if wiring_layout == 'bernds_wiring':
            self.wcl = bernds_wiring(self.WCA_WIDTH, self.WCA_HEIGHT)
        elif wiring_layout == 'christians_wiring':
            self.wcl = christians_wiring(self.WCA_WIDTH, self.WCA_HEIGHT)
        elif wiring_layout == 'timos_wiring':
            self.wcl = timos_wiring(self.WCA_WIDTH, self.WCA_HEIGHT)
        elif wiring_layout == 'robins_wiring':
            self.LOG.info("Using Robins Wiring")
            self.wcl = robins_wiring(self.WCA_WIDTH, self.WCA_HEIGHT)
        else:
            self.LOG.warn("No valid wiring layout found. Falling back to default!")
            self.LOG.info("Using default: Robins Wiring")
            self.wcl = robins_wiring(self.WCA_WIDTH, self.WCA_HEIGHT)

    def setColorBy1DCoordinates(self, strip, ledCoordinates, color):
        '''
        Linear mapping from top-left to bottom right
        '''
        self.LOG.debug("Ledcoordinates: " + str(ledCoordinates))

        for i in ledCoordinates:
            x, y = self.convert_1d_to_2d(i)

            self.LOG.debug("Setting coordinates: " + "(" + str(x) + "," + str(y) + ")")

            self.setColorBy2DCoordinates(strip, x, y, color)

    @staticmethod
    def convert_1d_to_2d(i):
        y = math.floor(i / 10)

        relative = i % 10

        if y % 2 == 0:
            x = 9 - relative
        else:
            x = relative

        return x, y

    def setColorBy2DCoordinates(self, strip, x, y, color):
        '''
        Mapping coordinates to the wordclocks display
        Needs hardware/wiring dependent implementation
        Final range:
             (0,0): top-left
             (self.WCA_WIDTH-1, self.WCA_HEIGHT-1): bottom-right
        '''
        strip_index = self.wcl.getStripIndexFrom2D(x, y)
        strip.setPixelColor(strip_index, color)

    def getStripIndexFrom2D(self, x, y):
        return self.wcl.getStripIndexFrom2D(x, y)

    def mapMinutes(self, min):
        '''
        Access minutes (1,2,3,4)
        '''
        return self.wcl.mapMinutes(min)


class robins_wiring:
    '''
    A class, holding all information of the wordclock's layout to map given
    timestamps, 2d-coordinates to the corresponding LEDs (corresponding to
    the individual wiring/layout of any wordclock).
    If a different wordclock wiring/layout is chosen, this class needs to be
    adopted.
    '''

    def __init__(self, WCA_WIDTH, WCA_HEIGHT):
        self.WCA_WIDTH = WCA_WIDTH
        self.WCA_HEIGHT = WCA_HEIGHT
        self.LED_COUNT = self.WCA_WIDTH * self.WCA_HEIGHT

    def getStripIndexFrom2D(self, x, y):
        '''
        Mapping coordinates to the wordclocks display
        Needs hardware/wiring dependent implementation
        Final range:
             (0,0): top-left
             (self.WCA_WIDTH-1, self.WCA_HEIGHT-1): bottom-right
        '''
        if y % 2 == 0:
            return int(y * self.WCA_WIDTH + (self.WCA_HEIGHT - x))
        else:
            return int(y * self.WCA_WIDTH + x)


class bernds_wiring:
    '''
    A class, holding all information of the wordclock's layout to map given
    timestamps, 2d-coordinates to the corresponding LEDs (corresponding to
    the individual wiring/layout of any wordclock).
    If a different wordclock wiring/layout is chosen, this class needs to be
    adopted.
    '''

    def __init__(self, WCA_WIDTH, WCA_HEIGHT):
        self.WCA_WIDTH = WCA_WIDTH
        self.WCA_HEIGHT = WCA_HEIGHT
        self.LED_COUNT = self.WCA_WIDTH * self.WCA_HEIGHT + 4

        # Logger
        level = logging.getLevelName('WARN')
        self.LOG = logging.getLogger(__name__)
        self.LOG.setLevel(level)

    def getStripIndexFrom2D(self, x, y):
        '''
        Mapping coordinates to the wordclocks display
        Needs hardware/wiring dependent implementation
        Final range:
             (0,0): top-left
             (self.WCA_WIDTH-1, self.WCA_HEIGHT-1): bottom-right
        '''
        if x % 2 == 0:
            pos = (self.WCA_WIDTH - x - 1) * self.WCA_HEIGHT + y + 2
        else:
            pos = (self.WCA_WIDTH * self.WCA_HEIGHT) - (self.WCA_HEIGHT * x) - y + 1
        return pos

    def mapMinutes(self, min):
        '''
        Access minutes (1,2,3,4)
        Needs hardware/wiring dependent implementation
        This implementation assumes the minutes to be wired as first and last two leds of the led-strip
        '''
        if min == 1:
            return self.LED_COUNT - 1
        elif min == 2:
            return 1
        elif min == 3:
            return self.LED_COUNT - 2
        elif min == 4:
            return 0
        else:
            self.LOG.warn('Out of range, when mapping minutes...')
            self.LOG.debug("Minutes: " + str(min))
            return 0


class christians_wiring:
    '''
    A class, holding all information of the wordclock's layout to map given
    timestamps, 2d-coordinates to the corresponding LEDs (corresponding to
    the individual wiring/layout of any wordclock).
    If a different wordclock wiring/layout is chosen, this class needs to be
    adopted.
    '''

    def __init__(self, WCA_WIDTH, WCA_HEIGHT):
        self.WCA_WIDTH = WCA_WIDTH
        self.WCA_HEIGHT = WCA_HEIGHT
        self.LED_COUNT = self.WCA_WIDTH * self.WCA_HEIGHT + 4

        # Logger
        level = logging.getLevelName('WARN')
        self.LOG = logging.getLogger(__name__)
        self.LOG.setLevel(level)

    def getStripIndexFrom2D(self, x, y):
        '''
        Mapping coordinates to the wordclocks display
        Needs hardware/wiring dependent implementation
        Final range:
             (0,0): top-left
             (self.WCA_WIDTH-1, self.WCA_HEIGHT-1): bottom-right
        '''
        if y % 2 == 0:
            pos = (self.WCA_HEIGHT - y - 1) * self.WCA_WIDTH + x
        else:
            pos = (self.WCA_HEIGHT * self.WCA_WIDTH) - (self.WCA_WIDTH * y) - x - 1
        return pos

    def mapMinutes(self, min):
        '''
        Access minutes (1,2,3,4)
        Needs hardware/wiring dependent implementation
        This implementation assumes the minutes to be wired as the last four leds of the led-strip
        '''
        if min == 1:
            return self.LED_COUNT - 4
        elif min == 2:
            return self.LED_COUNT - 3
        elif min == 3:
            return self.LED_COUNT - 2
        elif min == 4:
            return self.LED_COUNT - 1
        else:
            self.LOG.warn('Out of range, when mapping minutes...')
            self.LOG.debug("Minutes: " + str(min))
            return 0


class timos_wiring:
    '''
    A class, holding all information of the wordclock's layout to map given
    timestamps, 2d-coordinates to the corresponding LEDs (corresponding to
    the individual wiring/layout of any wordclock).
    If a different wordclock wiring/layout is chosen, this class needs to be
    adopted.
    '''

    def __init__(self, WCA_WIDTH, WCA_HEIGHT):
        self.WCA_WIDTH = WCA_WIDTH
        self.WCA_HEIGHT = WCA_HEIGHT
        self.LED_COUNT = self.WCA_WIDTH * self.WCA_HEIGHT + 4

        # Logger
        level = logging.getLevelName('WARN')
        self.LOG = logging.getLogger(__name__)
        self.LOG.setLevel(level)

    def getStripIndexFrom2D(self, x, y):
        '''
        Mapping coordinates to the wordclocks display
        Needs hardware/wiring dependent implementation
        Final range:
             (0,0): top-left
             (self.WCA_WIDTH-1, self.WCA_HEIGHT-1): bottom-right
        '''
        if x % 2 == 0:  # even columns 0,2,4,6,8,10
            pos = (x) * self.WCA_HEIGHT + y + 2  # last +2 for the minute LEDs before the WCA
        else:  # odd columns 1,3,5,7,9
            pos = (self.WCA_HEIGHT) + (self.WCA_HEIGHT * x) - y + 1
        return pos

    def mapMinutes(self, min):
        '''
        Access minutes (1,2,3,4)
        Needs hardware/wiring dependent implementation
        This implementation assumes the minutes to be wired as first and last two leds of the led-strip
        '''
        if min == 1:
            return 1
        elif min == 2:
            return self.LED_COUNT - 1
        elif min == 3:
            return self.LED_COUNT - 2
        elif min == 4:
            return 0
        else:
            self.LOG.warn('Out of range, when mapping minutes...')
            self.LOG.debug("Minutes: " + str(min))
            return 0
