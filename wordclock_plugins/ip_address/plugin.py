import os
import netifaces

import logging


class plugin:
    '''
    A class to restart the RPI
    '''

    def __init__(self, config):
        '''
        Initializations for the startup of the current wordclock plugin
        '''
        # Get plugin name (according to the folder, it is contained in)
        self.name = os.path.dirname(__file__).split('/')[-1]
        self.interface = config.get('plugin_' + self.name, 'interface')
        # Logger
        level = logging.getLevelName(config.get('logger', 'logging_level'))
        self.LOG = logging.getLogger(__name__)
        self.LOG.setLevel(level)

    def run(self, wcd, wci):
        '''
        Show ip of the wordclock
        '''
        try:
            ip = netifaces.ifaddresses(self.interface)[2][0]['addr']
            ip_suffix = ip.split('.')[-1]
            wcd.showText(ip_suffix)
        except Exception, e:
            wcd.showText('No ip.')
            self.LOG.error(e)

        return
