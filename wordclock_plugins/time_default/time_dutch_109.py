import logging


class time_dutch_10_9():
    '''
    This class returns a given time as a range of LED-indices.
    Illuminating these LEDs represents the current time on a dutch WCA
    range(23,26) +  range(20,22): HET IS

    range(27, 31) + range(20, 24): VIJF OVER
    [31, 32, 33, 16] + range(20, 24): TIEN OVER
    range(37, 42) + range(20, 24): KWART OVER
    [31, 32, 33, 16] + range(31, 35) + range(35, 39): TIEN VOOR HALF
    range(27, 31) + range(31, 35) + range(35, 39): VIJF VOOR HALF
    range(35, 39): HALF
    range(27, 31) + range(20, 24) + range(35, 39): VIJF OVER HALF
    [31, 32, 33, 16] + range(20, 24) + range(35, 39): TIEN OVER HALF
    range(37, 42) + range(31, 35): KWART VOOR
    [31, 32, 33, 16] + range(31, 35): TIEN VOOR
    range(27, 31) + range(31, 35): VIJF VOOR

    [78,79,80,81,10,7]: TWAALF
    range(87,90): EEN
    range(51,55): TWEE
    range(58,62): DRIE
    range(74,78): VIER
    range(83,87): VIJF
    [50,13,4]: ZES
    range(68,73): ZEVEN
    range(54,58): ACHT
    [68,67,66,11,6]: NEGEN
    [64,65,12,5]: TIEN
    range(61,64): ELF

 self.full_hour= [82,9,8]
    '''
    
    _w = {
        "HET": range(7, 10),
        "IS": range(4, 6),
        "VIJF": range(11, 15),
        "OVER": range(20, 24),
        "TIEN": range(15, 19),
        "KWART": range(25, 30),
        "VOOR": range(31, 35),
        "HALF": range(35, 39),
        "ACHT": range(46, 50),
        "TWEE": range(43, 47),
        "ZES": range(40, 43),
        "DRIE": range(50, 54),
        "ELF": range(53, 56),
        "TIEN_hour": range(56, 60),
        "ZEVEN": range(64, 69),
        "NEGEN": range(60, 65),
        "VIER": range(70, 74),
        "TWAALF": range(74, 80),
        "EEN": range(87, 90),
        "VIJF_hour": range(83, 87),
        "UUR": range(80, 83)
    }

    def __init__(self):

        # Logger
        level = logging.getLevelName('DEBUG')
        self.LOG = logging.getLogger(__name__)
        self.LOG.setLevel(level)
        
        self.prefix = self._w["HET"] + self._w["IS"]
        self.minutes = [[],
                        self._w["VIJF"] + self._w["OVER"],
                        self._w["TIEN"] + self._w["OVER"],
                        self._w["KWART"] + self._w["OVER"],
                        self._w["TIEN"] + self._w["VOOR"] + self._w["HALF"],
                        self._w["VIJF"] + self._w["VOOR"] + self._w["HALF"],
                        self._w["HALF"],
                        self._w["VIJF"] + self._w["OVER"] + self._w["HALF"],
                        self._w["TIEN"] + self._w["OVER"] + self._w["HALF"],
                        self._w["KWART"] + self._w["VOOR"],
                        self._w["TIEN"] + self._w["VOOR"],
                        self._w["VIJF"] + self._w["VOOR"]]
        self.hours = [self._w["TWAALF"],
                        self._w["EEN"],
                        self._w["TWEE"],
                        self._w["DRIE"],
                        self._w["VIER"],
                        self._w["VIJF_hour"],
                        self._w["ZES"],
                        self._w["ZEVEN"],
                        self._w["ACHT"],
                        self._w["NEGEN"],
                        self._w["TIEN_hour"],
                        self._w["ELF"],
                        self._w["TWAALF"]]
        self.full_hour = self._w["UUR"]

    def get_time(self, time, withPrefix=True):

        self.LOG.debug("Current time is: " + str(time))

        if time.minute == 18 or time.minute == 19:
            hour = time.hour % 12 + 1
        else:
            hour = time.hour % 12 + (1 if time.minute / 5 >= 4 else 0)

        minute = 0 if (time.minute / 5 if time.minute % 5 < 3 else (time.minute / 5) + 1) == 12 else (time.minute / 5 if time.minute % 5 < 3 else (time.minute / 5) + 1)

        full_time = (self.prefix if withPrefix else []) + \
                    self.minutes[minute] + \
                    self.hours[hour] + \
                    (self.full_hour if (minute == 0) else [])

        self.LOG.debug("LED strip indices being set: " + str(full_time))

        # Assemble indices
        return full_time
