import logging
import os
import time

import pywapi

import wordclock_tools.wordclock_colors as wcc


class plugin:
    '''
    A class to display the expected weather for a given location.
    Uses pywapi to retrieve information...
    '''

    def __init__(self, config):
        '''
        Initializations for the startup of the weather forecast
        '''
        # Get plugin name (according to the folder, it is contained in)
        self.name = os.path.dirname(__file__).split('/')[-1]
        self.location_id = config.get('plugin_' + self.name, 'location_id')
        self.weather_service = config.get('plugin_weather_forecast', 'weather_service')

        # Logger
        level = logging.getLevelName(config.get('logger', 'logging_level'))
        self.LOG = logging.getLogger(__name__)
        self.LOG.setLevel(level)

        try:
            import am2302_ths
            self.pin_temp_sensor = int(config.get('wordclock_interface', 'pin_temp_sensor'))
            self.temp_sensor_registered = True

            self.LOG.info("Registered temperature sensor at pin" + str(self.pin_temp_sensor) + '.')
        except Exception, e:
            self.LOG.info("No temperature sensor registered.")
            self.LOG.error(e)
            self.temp_sensor_registered = False

    def run(self, wcd, wci):
        '''
        Displaying expected temperature
        '''
        # Get current forecast
        if self.weather_service == 'yahoo':
            current_weather_forecast = pywapi.get_weather_from_yahoo(self.location_id)
            pywapi.get_weather_from_yahoo()
        elif self.weather_service == 'weather_dot_com':
            current_weather_forecast = pywapi.get_weather_from_weather_com(self.location_id)
        else:
            self.LOG.warn('Warning: No valid weather_forecast found!')

            return

        outdoor_temp = current_weather_forecast['current_conditions']['temperature']
        if self.temp_sensor_registered:
            try:
                indoor_temp = str(int(round(am2302_ths.get_temperature(4))))
                wcd.showText(outdoor_temp + '*', count=1, fps=8)
                wcd.showText(indoor_temp + '*', count=1, fg_color=wcc.GREEN, fps=8)
                wcd.showText(outdoor_temp + '*', count=1, fps=8)
                wcd.showText(indoor_temp + '*', count=1, fg_color=wcc.GREEN, fps=8)
            except Exception, e:
                self.LOG.error("Failed to read temperature sensor!")
                self.LOG.error(e)

                wcd.showText(outdoor_temp + '*   ' + outdoor_temp + '*   ' + outdoor_temp + '*', count=1, fps=8)
        else:
            wcd.showText(outdoor_temp + '*   ' + outdoor_temp + '*   ' + outdoor_temp + '*', count=1, fps=8)

        time.sleep(1)
