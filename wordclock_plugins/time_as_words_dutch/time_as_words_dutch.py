class time_as_words_dutch():
    '''
    This class returns a given time as words (string)::
    '''

    def __init__(self):
        self.prefix = "HET IS "
        self.minutes = ["",
                        "VIJF OVER ", \
                        "TIEN OVER ", \
                        "KWART OVER ", \
                        "TWINTIG OVER ", \
                        "VIJF VOOR HALF ", \
                        "HALF ", \
                        "VIJF OVER HALF ", \
                        "TWINTIG VOOR ", \
                        "KWART VOOR ", \
                        "TIEN VOOR ", \
                        "VIJF VOOR "]
        self.hours = ["TWAALF", \
                      "EEN", \
                      "TWEE", \
                      "DRIE", \
                      "VIER", \
                      "VIJF", \
                      "ZES", \
                      "ZEVEN", \
                      "ACHT", \
                      "NEGEN", \
                      "TIEN", \
                      "ELF", \
                      "TWAALF"]
        self.full_hour_suffix = [" UUR"]

    def get_time(self, time, withPrefix=True):
        if time.minute == 18 or time.minute == 19:
            hour = time.hour % 12 + 1
        else:
            hour = time.hour % 12 + (1 if time.minute / 5 >= 4 else 0)

        minute = 0 if (time.minute / 5 if time.minute % 5 < 3 else (time.minute / 5) + 1) == 12 else (time.minute / 5 if time.minute % 5 < 3 else (time.minute / 5) + 1)

        full_time = (self.prefix if withPrefix else []) + \
                    self.minutes[minute] + \
                    self.hours[hour] + \
                    (" UUR" if (minute == 0) else "")

        # Assemble indices
        return str(full_time)